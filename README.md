# go-todo-example (docker-compose version)
A simple RESTful API in Go. (Original repo is [here](https://github.com/jaredpiedt/go-todo-example))

## Run
API listen on port 8080. Or run `start.sh` directly.
```
docker-compose up -d
```
To cleanup, run `docker-compose down` to remove containers or `docker-compose down -v` to remove db data all together.

## API
This API provides the following endpoints:
```
POST    /items
DELETE  /items/{itemID}
GET     /items/{itemID}
PUT     /items/{itemID}
```