FROM golang:1.10-alpine

WORKDIR /go/src/bitbucket.org/royshum93/go-test

COPY . /go/src/bitbucket.org/royshum93/go-test
RUN apk add --no-cache curl git \
    && curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh \
    && dep ensure \
    && rm /go/bin/dep \
    && apk del curl git \
    && go build -o ./bin/server

ENTRYPOINT "./bin/server"
